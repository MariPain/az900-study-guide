## Guía de Estudio

Link a la formación oficial de Microsoft [Certificación Microsoft: AZ-900 Fundamentals](https://learn.microsoft.com/en-us/certifications/azure-fundamentals/)

### Módulo 1: Cloud Concepts (Conceptos en la Nube)

#### Cloud Computing (Computación en la Nube)
- **Definición**: Modelo que permite acceder a recursos informáticos a través de internet bajo demanda.
- **Ejemplo**: Utilización de servicios como Azure para alojar aplicaciones y almacenar datos sin necesidad de infraestructura física local.

#### Cloud Benefits (Beneficios de la Nube)
- **Definición**: Ventajas de la computación en la nube, incluyendo escalabilidad, pago por uso y acceso global.
  
##### Escalabilidad
- **Definición**: Capacidad de una aplicación o servicio para manejar un aumento o disminución en la demanda, añadiendo o eliminando recursos automáticamente.
- **Ejemplo**: Escalado automático de recursos en Azure para manejar picos de tráfico durante eventos de ventas.

##### Elasticidad
- **Definición**: Propiedad de la nube que permite escalar recursos hacia arriba o hacia abajo de forma automática según las necesidades, en tiempo real.
- **Ejemplo**: Ajuste dinámico de recursos de almacenamiento en Azure para adaptarse a cambios en el volumen de datos.

#### Cloud Service Types (Tipos de Servicios en la Nube)

##### IaaS (Infrastructure as a Service)
  - **Definición**: Modelo donde se proporciona infraestructura de TI virtualizada a través de internet.
  - **Ejemplo**: Azure Virtual Machines, donde se puede configurar y administrar máquinas virtuales según las necesidades.

##### PaaS (Platform as a Service)
  - **Definición**: Modelo donde se proporciona una plataforma completa de desarrollo y despliegue de aplicaciones.
  - **Ejemplo**: Azure App Service, que permite a los desarrolladores crear, desplegar y escalar aplicaciones web de manera rápida y sencilla.


##### SaaS (Software as a Service)
  - **Definición**: Modelo donde se proporciona software alojado y gestionado a través de la nube.
  - **Ejemplo**: Microsoft Office 365, que ofrece aplicaciones de productividad como Word, Excel y Outlook a través de la nube, sin necesidad de instalaciones locales.

#### Tipos de Nube
- **Nube Pública**
  - **Definición**: Infraestructura de nube ofrecida a través de servicios públicos de computación, como Azure, disponibles para cualquier persona o empresa que desee utilizarlos.
  - **Ejemplo**: Azure como servicio público de Microsoft, accesible globalmente a través de internet.

- **Nube Privada**
  - **Definición**: Infraestructura de nube utilizada exclusivamente por una sola organización, gestionada de forma interna o por un tercero.
  - **Ejemplo**: Implementación de servicios de nube en los propios centros de datos de una empresa, no accesibles al público en general.

- **Nube Híbrida**
  - **Definición**: Entorno informático que combina servicios y datos tanto de nubes públicas como privadas, permitiendo la portabilidad de datos y aplicaciones entre ellas.
  - **Ejemplo**: Utilización de Azure para cargas de trabajo críticas y locales para otras cargas de trabajo menos críticas dentro de una misma organización.

### Módulo 2: Azure Architecture and Services (Arquitectura y Servicios de Azure)

#### Core Azure Architectural Components (Componentes Arquitectónicos Centrales de Azure)

#### Azure Regions
- **Definición**: Una región de Azure es un área geográfica específica en la que Microsoft ha desplegado centros de datos. Cada región es independiente y está compuesta por uno o más centros de datos.
- **Ejemplo**: East US, West Europe, Southeast Asia.

#### Region Pairs
- **Definición**: Las regiones de Azure se agrupan en pares de regiones geográficamente separadas por al menos 300 millas para redundancia y recuperación ante desastres.
- **Ejemplo**: West US y East US, North Europe y West Europe.

#### Sovereign Regions
- **Definición**: Regiones específicas que almacenan datos bajo la jurisdicción legal de un país o región.
- **Ejemplo**: Azure Government, Azure China, Azure Germany.

#### Zonas de Disponibilidad
- **Definición**: Áreas dentro de una región de Azure con infraestructura física separada para resistencia ante fallas.
- **Ejemplo**: Zone 1, Zone 2, Zone 3 dentro de una región.

#### Datacenters de Azure
- **Definición**: Instalaciones físicas que albergan servidores y equipos de red utilizados para ejecutar servicios de Azure.
- **Ejemplo**: Datacenter en East US, Datacenter en West Europe.

#### Recursos y Grupos de Recursos de Azure
- **Definición**: Un recurso en Azure es cualquier servicio que se puede crear y administrar, como una máquina virtual, una base de datos o un sitio web. Los grupos de recursos son contenedores lógicos que mantienen recursos relacionados.
- **Ejemplo**: Creación de una máquina virtual (recurso) en un grupo de recursos llamado "MyResourceGroup".

#### Grupos de Administración
- **Definición**: Agrupación de suscripciones de Azure para aplicar políticas de gobernanza y administración a gran escala.
- **Ejemplo**: Creación de un grupo de administración para aplicar políticas de seguridad a múltiples suscripciones.

#### Jerarquía de Recursos, Suscripciones y Grupos de Administración
- **Definición**: Estructura organizativa para la gestión y administración de recursos en Azure, donde los recursos están contenidos en grupos de recursos, que a su vez están dentro de suscripciones, y estas pueden estar organizadas en grupos de administración.
- **Ejemplo**: Una empresa tiene múltiples suscripciones, cada una con varios grupos de recursos organizados bajo un grupo de administración central.

**Azure Compute and Networking Services (Servicios de Cómputo y Redes en Azure)**

#### Azure Virtual Machines
- **Definición**: Máquinas virtuales escalables y seguras bajo demanda.
- **Ejemplo de Uso**: Crear y ejecutar máquinas virtuales Windows o Linux en Azure para alojar aplicaciones, servidores y servicios.

#### Azure App Service
- **Definición**: Plataforma para desarrollar, implementar y escalar aplicaciones web y móviles.
- **Ejemplo de Uso**: Desplegar una aplicación web en Azure App Service para ofrecer servicios de backend escalables y gestionar el ciclo de vida de las aplicaciones.

#### Azure Virtual Network
- **Definición**: Red privada virtual en Azure para conectar recursos de Azure de manera segura y aislada.
- **Ejemplo de Uso**: Configurar una red virtual en Azure para conectar máquinas virtuales, aplicaciones y servicios desplegados en diferentes regiones o entornos, manteniendo la seguridad y la privacidad de los datos.

**Azure Storage Services (Servicios de Almacenamiento en Azure)**

#### Azure Blob Storage
- **Definición**: Servicio de almacenamiento en la nube diseñado para almacenar grandes cantidades de datos no estructurados como imágenes y archivos de texto.
- **Ejemplo de Uso**: Almacenar imágenes de perfil de usuario para una aplicación web, permitiendo el acceso rápido y seguro a través de una URL pública.

#### Azure Files
- **Definición**: Servicio de almacenamiento en la nube que proporciona almacenamiento compartido basado en archivos accesible como un recurso compartido de red estándar.
- **Ejemplo de Uso**: Implementar un sistema de archivos compartido para que múltiples máquinas virtuales en Azure puedan acceder y editar archivos de configuración y datos de aplicación de forma centralizada.

**Azure Identity, Access, and Security (Identidad, Acceso y Seguridad en Azure)**

#### Azure Active Directory (Azure AD)
- **Definición**: Servicio de identidad y acceso basado en la nube que proporciona gestión de identidades y autenticación para aplicaciones en la nube y en las instalaciones.
- **Ejemplo de Uso**: Centralizar y gestionar el acceso a aplicaciones SaaS como Microsoft 365, Salesforce y otras aplicaciones empresariales.

#### Role-Based Access Control (RBAC)
- **Definición**: Control de acceso basado en roles para recursos de Azure que permite asignar permisos a usuarios y grupos a nivel de recurso.
- **Ejemplo de Uso**: Asignar roles específicos como administrador, lector o colaborador a usuarios o grupos para restringir  o permitir acciones sobre recursos en Azure.

#### Azure Security Center
- **Definición**: Servicio que proporciona monitoreo y mejora continua de la seguridad de las cargas de trabajo en Azure, así como recomendaciones de seguridad basadas en la configuración y el comportamiento de las máquinas virtuales y otros recursos.
- **Ejemplo de Uso**: Configurar políticas de seguridad personalizadas para garantizar el cumplimiento de los estándares de seguridad y realizar análisis continuos de la postura de seguridad en Azure.

### Módulo 3: Core Solutions (Soluciones Centrales)

#### Azure Cost Management
- **Definición**: Herramienta para controlar y optimizar costos en Azure mediante informes detallados, presupuestos y alertas.
- **Ejemplo de Uso**: Monitorizar el gasto mensual en recursos de cómputo y almacenamiento en Azure, estableciendo alertas para evitar sobrecostos inesperados.

#### Azure Reservations
- **Definición**: Programa que ofrece descuentos significativos por compromiso de uso a largo plazo en recursos como máquinas virtuales y bases de datos en Azure.
- **Ejemplo de Uso**: Comprar reservas de máquinas virtuales para un año o más, beneficiándose de descuentos por previsión de uso constante y reduciendo el coste operativo total.

**Features and Tools in Azure for Governance and Compliance (Herramientas de Azure para Gobernanza y Cumplimiento)**

#### Azure Policy
- **Definición**: Servicio que permite implementar y gestionar políticas para la administración de recursos y cumplimiento en Azure.
- **Ejemplo de Uso**: Establecer políticas que requieran el uso de discos cifrados para todas las máquinas virtuales en una suscripción de Azure.

#### Azure Blueprints
- **Definición**: Herramienta para definir y desplegar entornos de Azure predefinidos y políticas asociadas a través de plantillas reutilizables.
- **Ejemplo de Uso**: Crear un blueprint que incluya la configuración de redes virtuales, grupos de seguridad de red y políticas de seguridad estándar para aplicar a todas las nuevas implementaciones de recursos en Azure.

**Feature and Tools for Managing and Deploying Azure Resources (Herramientas de Gestión e Implementación de Recursos en Azure)**

#### Azure Resource Manager (ARM)
- **Definición**: Servicio de orquestación que facilita la implementación, administración y supervisión de recursos en Azure.
- **Ejemplo de Uso**: Desplegar una aplicación compuesta por máquinas virtuales, bases de datos y servicios web mediante plantillas de ARM para asegurar la consistencia y la automatización del proceso.

#### Azure CLI y Azure PowerShell
- **Definición**: Herramientas de línea de comandos para administrar y automatizar recursos en Azure desde una interfaz de línea de comandos.
- **Ejemplo de Uso**: Crear y configurar recursos como máquinas virtuales, redes virtuales y grupos de seguridad utilizando comandos en Azure CLI o Azure PowerShell para una gestión eficiente y repetible.

**Monitoring Tools in Azure (Herramientas de Monitoreo en Azure)**

#### Azure Monitor
- **Definición**: Servicio centralizado para monitoreo y diagnóstico de aplicaciones y recursos en Azure, ofreciendo visibilidad en tiempo real y análisis detallado del rendimiento y la disponibilidad.
- **Ejemplo de Uso**: Configurar alertas personalizadas para detectar y responder automáticamente a anomalías de rendimiento en máquinas virtuales y servicios de Azure.

#### Azure Application Insights
- **Definición**: Herramienta avanzada de monitoreo que proporciona información detallada sobre el rendimiento y el uso de aplicaciones web y móviles en tiempo real.
- **Ejemplo de Uso**: Analizar el rendimiento de una aplicación web en términos de tiempos de respuesta, cargas de página y errores para identificar áreas de mejora y optimización.

#### Log Analytics
- **Definición**: Servicio que permite realizar análisis avanzados y visualización de datos de registros de aplicaciones y recursos en Azure para troubleshooting, análisis de tendencias y cumplimiento.
- **Ejemplo de Uso**: Configurar consultas de búsqueda en registros para identificar patrones de comportamiento o anomalías en el tráfico de red y las actividades de seguridad.

 
 ### Esquema Visual de Conceptos Clave de Azure con Enlaces a Certificaciones de Microsoft (AZ-900)

 [Certificación Microsoft: AZ-900 ](https://learn.microsoft.com/en-us/credentials/certifications/azure-fundamentals/?practice-assessment-type=certification)


 Veamos definiciones con ejemplos:

#### Identity Access and Security
- **Microsoft Entra ID (Azure AD)**
  - **Definición**: Servicio de identidad en la nube que proporciona autenticación y autorización para aplicaciones y servicios.
  - **Ejemplo**: Administrar accesos de empleados a aplicaciones corporativas.
  - **Enlace**: [Learn about Microsoft Entra ID (Azure AD)](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/5-describe-core-architectural-components-of-azure)
  
- **Microsoft Entra Domain Services**
  - **Definición**: Proporciona servicios de dominio gestionados como unión a dominio, LDAP, autenticación Kerberos, etc.
  - **Ejemplo**: Habilitar join de máquinas virtuales a un dominio sin controladores de dominio locales.
  - **Enlace**: [Learn about Microsoft Entra Domain Services](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/5-describe-core-architectural-components-of-azure)
  
- **Role-Based Access Control (RBAC)**
  - **Definición**: Sistema de permisos granulares que permite la gestión de accesos y permisos en Azure.
  - **Ejemplo**: Asignar permisos de lectura a un grupo de usuarios en un grupo de recursos específico.
  - **Enlace**: [Learn about RBAC](https://learn.microsoft.com/en-us/learn/modules/secure-network-connectivity-in-azure/2-explore-azure-role-based-access-control)
  
- **Azure Multi-Factor Authentication (MFA)**
  - **Definición**: Añade una capa adicional de seguridad al requerir más de una forma de verificación.
  - **Ejemplo**: Requerir MFA para acceso a aplicaciones críticas desde ubicaciones no confiables.
  - **Enlace**: [Learn about Azure MFA](https://learn.microsoft.com/en-us/learn/modules/secure-your-azure-resources-with-rbac-and-policies/4-use-mfa-to-protect-azure-resources)
  
- **Azure Key Vault**
  - **Definición**: Servicio para almacenar y gestionar secretos, claves y certificados.
  - **Ejemplo**: Guardar y acceder a las claves de cifrado de una aplicación web.
  - **Enlace**: [Learn about Azure Key Vault](https://learn.microsoft.com/en-us/learn/modules/secure-your-azure-resources-with-rbac-and-policies/5-describe-azure-key-vault)
  
- **Conditional Access**
  - **Definición**: Herramienta para controlar cómo y cuándo los usuarios pueden acceder a los recursos.
  - **Ejemplo**: Permitir acceso a recursos de la empresa solo desde dispositivos conformes.
  - **Enlace**: [Learn about Conditional Access](https://learn.microsoft.com/en-us/learn/modules/secure-your-azure-resources-with-rbac-and-policies/3-implement-conditional-access)
  
- **B2B and B2C**
  - **Definición**: Servicios de Azure AD para la autenticación de usuarios externos y consumidores.
  - **Ejemplo**: Autenticación de usuarios finales en una aplicación pública.
  - **Enlace**: [Learn about B2B and B2C](https://learn.microsoft.com/en-us/learn/modules/secure-network-connectivity-in-azure/2-explore-azure-role-based-access-control)
  
- **Defense in Depth**
  - **Definición**: Estrategia de seguridad que implementa múltiples capas de defensa.
  - **Ejemplo**: Implementar firewalls, controles de acceso y monitoreo continuo.
  - **Enlace**: [Learn about Defense in Depth](https://learn.microsoft.com/en-us/learn/modules/secure-network-connectivity-in-azure/3-describe-defense-in-depth-strategy)
  
- **Microsoft Defender for Cloud**
  - **Definición**: Herramienta de gestión de seguridad que proporciona visibilidad y control sobre la seguridad de los recursos en la nube.
  - **Ejemplo**: Detectar y mitigar amenazas de seguridad en tiempo real.
  - **Enlace**: [Learn about Microsoft Defender for Cloud](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/5-describe-core-architectural-components-of-azure)

  [Certificación Microsoft: AZ-900 - Identity Access and Security](https://learn.microsoft.com/en-us/certifications/azure-fundamentals/)

#### Azure Cost Management
- **Factors Affecting Cost**
  - **Definición**: Variables que impactan el costo total de los servicios en Azure.
  - **Ejemplo**: El tipo de servicio utilizado, el uso de recursos y la ubicación de los recursos.
  - **Enlace**: [Learn about Factors Affecting Cost](https://learn.microsoft.com/en-us/learn/modules/plan-manage-azure-costs/2-factors-influencing-cost)
  
- **Azure Marketplace**
  - **Definición**: Plataforma para encontrar, probar y adquirir software y servicios de terceros.
  - **Ejemplo**: Implementar soluciones de software directamente desde el Azure Marketplace.
  - **Enlace**: [Learn about Azure Marketplace](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/6-explore-azure-pricing-and-support)
  
- **Azure Pricing Calculator**
  - **Definición**: Herramienta para estimar el costo de uso de los servicios de Azure.
  - **Ejemplo**: Calcular el costo estimado de ejecutar una aplicación web en Azure.
  - **Enlace**: [Learn about Azure Pricing Calculator](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/6-explore-azure-pricing-and-support)
  
- **TCO Calculator**
  - **Definición**: Herramienta para calcular el costo total de propiedad (TCO) al migrar a Azure.
  - **Ejemplo**: Comparar el TCO de una infraestructura local versus una infraestructura en Azure.
  - **Enlace**: [Learn about TCO Calculator](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/6-explore-azure-pricing-and-support)
  
- **Azure Cost Management**
  - **Definición**: Servicio para monitorear y gestionar los costos en Azure.
  - **Ejemplo**: Monitorear el uso y el costo de los recursos en tiempo real.
  - **Enlace**: [Learn about Azure Cost Management](https://learn.microsoft.com/en-us/learn/modules/plan-manage-azure-costs/4-manage-azure-costs)
  
- **Resource Tags**
  - **Definición**: Etiquetas aplicadas a los recursos de Azure para organizar y gestionar los costos.
  - **Ejemplo**: Aplicar etiquetas a los recursos para realizar un seguimiento del uso y los costos por departamento.
  - **Enlace**: [Learn about Resource Tags](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/6-explore-azure-pricing-and-support)

  [Certificación Microsoft: AZ-900 - Azure Cost Management](https://learn.microsoft.com/en-us/certifications/azure-fundamentals/)

#### Azure Governance and Compliance
- **Azure Policy**
  - **Definición**: Servicio para crear, asignar y gestionar políticas que impongan reglas y efectos sobre los recursos.
  - **Ejemplo**: Aplicar una política que requiere que todas las cuentas de almacenamiento utilicen cifrado.
  - **Enlace**: [Learn about Azure Policy](https://learn.microsoft.com/en-us/learn/modules/secure-your-azure-resources-with-rbac-and-policies/2-implement-azure-policy)
  
- **Resource Locks**
  - **Definición**: Función que permite bloquear recursos para evitar su eliminación accidental.
  - **Ejemplo**: Bloquear un grupo de recursos crítico para que no pueda ser eliminado.
  - **Enlace**: [Learn about Resource Locks](https://learn.microsoft.com/en-us/learn/modules/secure-your-azure-resources-with-rbac-and-policies/2-implement-azure-policy)
  
- **Service Trust Portal**
  - **Definición**: Portal que proporciona información sobre la seguridad, privacidad y cumplimiento de Azure.
  - **Ejemplo**: Acceder a auditorías y certificaciones de cumplimiento de Azure.
  - **Enlace**: [Learn about Service Trust Portal](https://learn.microsoft.com/en-us/learn/modules/secure-your-azure-resources-with-rbac-and-policies/5-describe-azure-key-vault)
  
- **Microsoft Purview**
  - **Definición**: Servicio de gobernanza de datos y cumplimiento para la gestión y protección de datos.
  - **Ejemplo**: Implementar políticas de retención y protección de datos sensibles.
  - **Enlace**: [Learn about Microsoft Purview](https://learn.microsoft.com/en-us/learn/modules/secure-your-azure-resources-with-rbac-and-policies/2-implement-azure-policy)

  [Certificación Microsoft: AZ-900 - Azure Governance and Compliance](https://learn.microsoft.com/en-us/certifications/azure-fundamentals/)

#### Resources Deployment Tools
- **Azure Portal**
  - **Definición**: Interfaz web para gestionar y configurar los servicios de Azure.
  - **Ejemplo**: Crear y gestionar máquinas virtuale a través de una interfaz gráfica.
  - **Enlace**: [Learn about Azure Portal](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/5-describe-core-architectural-components-of-azure)
  
- **Azure PowerShell**
  - **Definición**: Módulo de PowerShell para automatizar y gestionar tareas en Azure.
  - **Ejemplo**: Desplegar una infraestructura completa con scripts de PowerShell.
  - **Enlace**: [Learn about Azure PowerShell](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/5-describe-core-architectural-components-of-azure)
  
- **Azure CLI**
  - **Definición**: Herramienta de línea de comandos para gestionar servicios de Azure.
  - **Ejemplo**: Ejecutar comandos CLI para crear y administrar recursos en Azure.
  - **Enlace**: [Learn about Azure CLI](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/5-describe-core-architectural-components-of-azure)
  
- **Azure Arc**
  - **Definición**: Servicio para gestionar y gobernar recursos en entornos híbridos y multi-nube.
  - **Ejemplo**: Gestionar servidores físicos y máquinas virtuales en diferentes nubes desde Azure.
  - **Enlace**: [Learn about Azure Arc](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/5-describe-core-architectural-components-of-azure)
  
- **Azure Resource Manager (ARM)**
  - **Definición**: Servicio que proporciona una capa de gestión para la creación, actualización y eliminación de recursos en Azure.
  - **Ejemplo**: Implementar plantillas ARM para despliegues repetibles de infraestructura.
  - **Enlace**: [Learn about ARM](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/5-describe-core-architectural-components-of-azure)

  [Certificación Microsoft: AZ-900 - Resources Deployment Tools](https://learn.microsoft.com/en-us/certifications/azure-fundamentals/)

#### Monitoring Tools
- **Azure Service Health**
  - **Definición**: Proporciona notificaciones personalizadas y soporte cuando los servicios de Azure tienen problemas.
  - **Ejemplo**: Recibir alertas cuando hay interrupciones de servicio que afectan a los recursos.
  - **Enlace**: [Learn about Azure Service Health](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/5-describe-core-architectural-components-of-azure)
  
- **Azure Advisor**
  - **Definición**: Ofrece recomendaciones personalizadas para optimizar las implementaciones de Azure.
  - **Ejemplo**: Recibir sugerencias para mejorar la seguridad y reducir costos.
  - **Enlace**: [Learn about Azure Advisor](https://learn.microsoft.com/en-us/learn/modules/plan-manage-azure-costs/4-manage-azure-costs)
  
- **Azure Monitor**
  - **Definición**: Servicio integral de monitoreo para recolectar, analizar y actuar sobre datos de telemetría.
  - **Ejemplo**: Monitorear el rendimiento de una aplicación web en tiempo real.
  - **Subservicios**:
    - **Azure Application Insights**
      - **Definición**: Herramienta para monitorear el rendimiento y uso de aplicaciones.
      - **Ejemplo**: Identificar y diagnosticar problemas de rendimiento en una aplicación web.
      - **Enlace**: [Learn about Azure Application Insights](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/5-describe-core-architectural-components-of-azure)
    - **Log Analytics**
      - **Definición**: Herramienta para recopilar y analizar registros de datos.
      - **Ejemplo**: Analizar registros de actividad y diagnóstico de múltiples fuentes.
      - **Enlace**: [Learn about Log Analytics](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/5-describe-core-architectural-components-of-azure)
    - **Azure Monitor Logs**
      - **Definición**: Almacena y consulta grandes volúmenes de datos de registro.
      - **Ejemplo**: Realizar consultas detalladas para identificar tendencias y anomalías en los registros.
      - **Enlace**: [Learn about Azure Monitor Logs](https://learn.microsoft.com/en-us/learn/modules/intro-to-azure-fundamentals/5-describe-core-architectural-components-of-azure)






